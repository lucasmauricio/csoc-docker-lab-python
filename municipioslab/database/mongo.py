from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

from ..settings import logger, system_config as config
from .main import Database
from time import sleep


class MongoConnector(Database):

    def connect(self):
        user = config["mongo_username"]
        password = config["mongo_password"]
        url = config["mongo_url"]
        connection = f"mongodb://{url}/?retryWrites=true&w=majority"
        logger.debug(f"Connecting in MongoDB ({connection})")
        # TODO use credentials
        # TODO check if is it necessary using these DB parameters
        self.client = MongoClient( #{user}:{password}@
            connection, serverSelectionTimeoutMS=500)
        for i in range(config["max_retries"]):
            try:
                self.client.admin.command('ismaster')
            except ConnectionFailure as err:
                logger.warning(err)
                logger.debug(f"Mongo unavailable, retrying in {config['retry_delay']}s ({i+1} of {config['max_retries']})")
                sleep(config["retry_delay"])
                continue
            else:
                logger.debug("Mongo available.")
                break
        
        self.db = self.client.cities_db

    def close(self):
        self.client.close()

    # city

    def insert_city(self, new_city):
        collection_id = "city"
        cities = self.db[collection_id]
        new_id = cities.insert_one(new_city).inserted_id
        return new_id

    def list_cities(self):
        collection_id = "city"
        cities = self.db[collection_id]
        result = []
        all_cities = cities.find()
        for item in all_cities:
            del(item['_id'])
            result.append(item)
        return result

    def get_city(self, number):
        collection_id = "city"
        cities = self.db[collection_id]
        res = cities.find_one({'id':number})
        if res:
            del (res['_id'])
        return res

    def delete_city(self, number):
        collection_id = "city"
        self.db[collection_id].delete_one({'city_number':number})
        #TODO Return ?
