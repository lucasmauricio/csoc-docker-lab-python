from .main import Database
from .mongo import MongoConnector
# from .memory import MemoryConnector


def build_database(dbms):
    if 'MemoryDB' == dbms:
        return Database() #MemoryConnector()
    elif 'MongoDB' == dbms:
        return MongoConnector()
    else:
        return Database()
