import logging
import os


def server_configuration():
    config = {}
    # mongo
    config["mongo_username"] = os.environ.get("MONGO_USER", "mongo")
    config["mongo_password"] = os.environ.get("MONGO_PASSWORD", "mongo")
    config["mongo_url"] = os.environ.get("MONGO_URL", "localhost:27017")
    config["max_retries"] = 3
    config["retry_delay"] = 2
    return config
system_config = server_configuration()


logging.basicConfig(
    format=("%(asctime)s,%(msecs)-3d - %(name)-12s - %(levelname)-8s => %(message)s"),
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.DEBUG
)
logger = logging.getLogger(__name__)
