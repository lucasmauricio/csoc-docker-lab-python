import requests
import logging
import json

logger = logging.getLogger(__name__)

def importar_municipios(database):
    """Buscar municípios da base do IBGE e persistir na base de dados deste serviço."""
    municipios_ibge = carregar_do_ibge()
    persistir(database, municipios_ibge)


def carregar_do_ibge():
    url_api_ibge = 'https://servicodados.ibge.gov.br/api/v1/localidades/municipios'

    timeout = 4
    headers = {'content-type': 'application/json', 'accept': 'application/json', 'content-type': 'application/json',
               'Connection': 'close'}
    response = requests.get(url=f'{url_api_ibge}', headers=headers, timeout=timeout)
    status = response.status_code in (200, 201)
    result = None
    if status:
        result = json.loads(response.content)
    return result


def persistir(database, municipios_ibge):
    database.connect()
    for municipio in municipios_ibge:
        logger.debug(municipio)
        database.insert_city(municipio)
