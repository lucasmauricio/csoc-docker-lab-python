# -*- coding: utf-8 -*-
import json
from flask import request, Flask
from datetime import datetime
import re
import logging

from ..settings import logger


global db


def validate_suported_mime_type():
    return True
    # if 'Accept' in request.headers:
    #     if request.headers['Accept'] == '*/*':
    #         return True
    #     else:
    #         return request.headers['Accept'] == 'application/json'
    # else:
    #     return True


# operations to be registered in the application object
app = Flask(__name__)


@app.route('/api/municipios/', methods=['GET'])
def get_all_cities_api():
    """API para listar todos os municípios"""
    db.connect()
    dados = db.list_cities()
    j = json.dumps(dados)
    return j, {'Content-Type': 'application/json; charset=utf-8'}


@app.route("/api/municipios/<int:cod_municipio>", methods = ['GET'])
def get_city_by_id_api(cod_municipio=None):
    """API para exibir um município dado o seu código"""
    if not validate_suported_mime_type():
        return "Unsupported Media Type", 415
    elif request.method == "GET":
        if cod_municipio:
            # retrieve one specific employee
            db.connect()
            dados = db.get_city(cod_municipio)
            logger.debug(f"Cidade encontrada: {dados}")
            if dados:
                return json.dumps(dados), {'Content-Type': 'application/json; charset=utf-8'}
            else:
                return "Not found", 404
        else:
            return "Bad Request", 400


def executar_servicos(database):
    global db
    db = database
    logger.info(f'Iniciando o servidor das APIs')
    app.run(host="0.0.0.0")
