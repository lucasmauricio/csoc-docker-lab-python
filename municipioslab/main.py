import click
# import logging
import time

from municipioslab import __version__ as app_version
from municipioslab.settings import logger, system_config
from municipioslab.ibge_importer import importar_municipios as import_IBGE_data
from municipioslab.services import executar_servicos
from municipioslab.database import build_database


# logging.basicConfig(
#     format=("%(asctime)s,%(msecs)-3d - %(name)-12s - %(levelname)-8s => %(message)s"),
#     datefmt="%Y-%m-%d %H:%M:%S",
#     level=logging.DEBUG
# )
# logger = logging.getLogger(__name__)


@click.command()
@click.option('-i', '--import_data', is_flag=True, help='Importar dados do IBGE.')
@click.option('-v', '--verbose', is_flag=True, help='Verbose mode.')
def start(import_data, verbose):
    """ Starts municipio API lab.
    """
    start_time = time.time()
    logger.info("Inicialização do Município API lab...")
    logger.info(f"Simulator version: {app_version}")
    logger.info(f"System configuration: {system_config}")

    database = build_database('MongoDB')
    logger.info(f"Base de dados inicializada: {database}")

    if import_data:
        import_IBGE_data(database)

    # logger.info(f"The simulation for payroll id {payroll_id} will run {len(phases)} "
    #             f"task(s) and will interact with {len(databases)} database(s).")

    end_time = time.time()
    logger.info(f'The simulation were executed in {end_time - start_time} seconds')

    executar_servicos(database)


if __name__ == "__main__":
    start()
