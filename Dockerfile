FROM python:3.8-slim-buster

# upgrading Debian packages and installing packages to support the application
#RUN apt-get update \
# && apt-get clean \
# && rm -rf /var/lib/apt/lists/*

# installing the application
WORKDIR /dist
COPY dist/municipioslab-*.tar.gz /dist/municipioslab.tar.gz

#RUN python3 -m pip install --proxy="" --upgrade pip
#RUN pip3 --version

RUN python3 -m pip install --no-cache-dir municipioslab.tar.gz

# setting environment variables with default values
# mongo envars
ENV MONGO_USER="mongo"
ENV MONGO_PASSWORD="mongo"
ENV MONGO_URL="localhost:27017"

EXPOSE 5000

CMD ["municipioslab", "-i"]
