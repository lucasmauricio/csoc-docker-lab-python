from setuptools import setup, find_packages
import io
import re


with io.open('./municipioslab/__init__.py', encoding='utf8') as version_file:
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file.read(), re.M)
    if version_match:
        version = version_match.group(1)
    else:
        raise RuntimeError("Unable to find version string.")


with io.open('README.md', encoding='utf8') as readme:
    long_description = readme.read()


setup(
    name="municipioslab",
    version=version,
    author='Lucas Martins',
    author_email='lucasmauricio@yahoo.com.br',
    packages=find_packages(exclude='tests'),
    long_description=long_description,
    url="",
    description="Aplicação construída em Python para ser usada no laboratório de Docker da CSCP.",
    install_requires=['pymongo', 'requests', 'Flask',
                      'pathlib', 'click', 'coverage'],
    entry_points={
        'console_scripts': [
            'municipioslab = municipioslab.main:start',
        ],
    },
    classifiers=[
        'Intended Audience :: Information Technology',
        'Intended Audience :: Science/Research',
        'Natural Language :: English',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Architecture',
    ],
    keywords=(
        'municípios do Brasil'
        'API municípios'
    )
)
